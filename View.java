/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*package tempui;*/

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *view class to word count, uses model class.
 * @author Eduardo Flores Bautista
 */
public class View extends JFrame {
    Model file = new Model();//creates a file model

    JButton convertButton = new JButton( "Count");

    JLabel answer = new JLabel ( "Unknown Words");

    JLabel total = new JLabel ( "Unknown Total");

    JButton fileButton = new JButton( ".txt File");
    JFileChooser fileChooser = new JFileChooser();
    /**
    *constructor method
    * @param  no parameters
    * @return void method, no return.
    */
    public View() {
        convertButton.setVisible(false);//dont allow for word count button to be visable at first.

        this.setTitle("Word Counter");
        this.setBounds( 200, 300, 350, 350);
        this.getContentPane().setLayout(null);
        this.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE);

        this.convertButton.setBounds(10, 80, 100, 30);
        this.getContentPane().add( convertButton);
        this.convertButton.addActionListener( new ButtonListener());

        this.answer.setBounds(10, 100, 1000, 100);
        this.getContentPane().add( answer);

        this.total.setBounds(10, 150, 1000, 100);
        this.getContentPane().add( total);

        this.fileButton.setBounds(200, 80, 100, 30);
        this.getContentPane().add(fileButton);
        this.fileButton.addActionListener( new FileButtonListener());

    }


    /**
     *view class to word count, uses model class.
     * @author Eduardo Flores Bautista
     */
    private class ButtonListener implements ActionListener {

        @Override
        public void actionPerformed( ActionEvent e) {
            View.this.theButtonHasBeenPushed();
        }
    }
    /**
     *button Listener class
     * @author Eduardo Flores Bautista
     */
    private class FileButtonListener implements ActionListener {
        @Override
        public void actionPerformed( ActionEvent e) {
            //hit file button
            JFileChooser chooser = new JFileChooser();
            chooser.setFileFilter( new FileNameExtensionFilter("TEXT FILES", "txt", "text"));
            //created file chooser
            int chooserSuccess = chooser.showOpenDialog( null);
            System.out.println("I opended the file chooser, it returned " + chooserSuccess);
            if( chooserSuccess == JFileChooser.APPROVE_OPTION) {
                File chosenFile = chooser.getSelectedFile();
                // Pass this file to your function
                file.setFile(chosenFile);
                convertButton.setVisible(true);
                System.out.println("You chose the file " + chosenFile.getAbsolutePath());
                System.out.println("You chose the file " + chosenFile.getName());
            }
            else {
                System.out.println("You hit cancel");
            }
        }
    }//end FileButtonListener
    /**
    *method for what is to happen when count buton is pushed.
    * @param  no paramaters
    * @return void method, no return.
    */
    private void theButtonHasBeenPushed() {
        file.countWords();//this also catches file not found exception
        String wordCount = "Words: "+this.file.getMap().toString();
        this.answer.setText(wordCount);
        String totalWords = "Total Words: "+this.file.getWordCount();
        this.total.setText(totalWords);

    }


}
