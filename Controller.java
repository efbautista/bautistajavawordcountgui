/*package tempui;*/
import java.io.* ;
import java.util.Scanner;
/**
 *controller class uses View and Model class functions.
 * @author efbautista
 */
public class Controller{
    public static void main(String []args) {
        View window = new View();
        window.setVisible(true);
    }

 }
