import java.io.* ;
import java.io.Reader;//needed for reading each line of text.
import java.util.Scanner;
import java.util.Collections;
import java.util.AbstractMap;
import java.util.HashMap;
import java.io.Reader;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.lang.String;
import java.io.FileInputStream;


/**
 *model class which represents text file.
 * @author Eduardo Flores Bautista
 */
public class Model{
    private File inFile;
    private int wordCount=0;
    //needed map to input the word and amount of times seen.
    private HashMap<String, Integer> myMap = new HashMap<String, Integer>();

   /**
   *constructor method
   * @param  File file
   * @return void method, no return.
   */
    public Model (File file ){
        this.inFile = file;
    }
    /**
    *constructor method
    * @param  no paramaters.
    * @return void method, no return.
    */
     public Model (){
         this.inFile = null;
     }
     /**
     *constructor method
     * @param  File file
     * @return void method, no return.
     */
      public void setFile (File file ){
          this.inFile = file;
      }
    /**
   *counts words in text file.
   * @param  no paramaters.
   * @return void method, no return.
   */
    public void countWords(){
        //first catch array out of bounds and file not found exception for file.
        Scanner scanner = new Scanner(System.in);
        try
    	{
        scanner = new Scanner(this.inFile);
        }
        catch (FileNotFoundException e)
        {
        System.out.println("The file was not found. Make sure the file is in project folder.");
        }

        /*while there is a word to scan*/
        while(scanner.hasNext() == true){
            String currentWord= scanner.next();
            //remove all punctuation from the scanned word
            currentWord = currentWord.replaceAll("\\p{Punct}","");
            //turn all words to lowercase
            currentWord=currentWord.toLowerCase();

            //now we can add the word into hashmap

            //before adding check to see if word is existing
                if(myMap.containsKey(currentWord)==false){
                    /*if the word is new in the map,
                    add the word to the count to 1*/
                    myMap.put(currentWord,1);
                    //incriment count.
                    wordCount++;
                }
                else{
                    myMap.put(currentWord, myMap.get(currentWord) + 1);
                    //incriment count
                    wordCount++;
                }

        }//end while
    }//end CountWords

    /**
   *prints myMap using .entrySet().
   * @param  no paramaters.
   * @return void method, no return.
   */
    public void printUnlisted(){

        System.out.println(myMap.entrySet());
    }//end printUnlisted
    /**
    *prints wordCount using.
    * @param  no paramaters
    * @return void method, no return.
    */
    public void printTotal(){
        System.out.println("total: "+ wordCount);
    }
    /**
    *returns HashMap of model.
    * @param  no paramaters
    * @return Map<String, Integer> .
    */
    public HashMap<String, Integer> getMap() {

        return this.myMap;
    }
    /**
    *returns wordCount.
    * @param  no paramaters
    * @return int.
    */
    public int getWordCount() {
        return this.wordCount;
    }









}//end Model
